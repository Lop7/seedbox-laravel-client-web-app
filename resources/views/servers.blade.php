@extends('layouts.app')

@section('content')

    <div class = "row center-align">
            <a href="{{route('home')}}" class = "btn red waves">Home</a>
            {{-- If no errors, display create/find button --}}
        @if($error == null)
            <a href="{{route('createForm')}}" class = "btn red waves">Create</a>
        @endif
    </div>
<div class = "row center-align">
    @if ($error !== null) 
        Something went wrong! Please reauthenticate!<br>
        <a class = "btn purple"href="{{route('manage')}}">Auth Page</a>
    @elseif(($error == null) && ($servers != null))
        <table class = "responsive-table">
            <tr>
                <th>ID</th>
                <th>Address</th>
                <th>Hostname</th>
                <th>Description</th>
                <th>Modify</th>
                <th>Remove</th>
            </tr>
            <tbody>
                {{-- {{$servers}} --}}
                @foreach($servers as $server)
                    <tr>
                        <td>{{$server->id or ''}}</td>
                        <td>{{$server->address or ''}}</td>
                        <td>{{$server->hostname or ''}}</td>
                        <td>{{str_limit($server->description, 15)}}</td>
                        <td><a class = "btn small blue" href="{{route('updateForm',$server->id)}}"><i class="material-icons">update</i></a></td>
                        <td><a class = "btn small red" href="{{route('deleteServer', $server->id)}}"><i class="material-icons">delete</i></a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        Nothingness!
    @endif
</div>
@endsection

