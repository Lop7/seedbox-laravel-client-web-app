@extends('layouts.app')

@section('content')
<div class = "row center-align">
    <a href="{{route('home')}}" class = "btn red waves">Home</a>
</div>
<div class = " row center-align">
    @isset($_COOKIE["access_token"]) 
        <div class = "row">
            <a class = "btn black" href="{{route('servers')}}">Manage Servers!!!</a>
        </div>
        <div class = "row">
            <a class = "btn indigo lighten-1" href="{{route('logout')}}">Logout!!!</a>
        </div>

    @else
        <a class = "btn black" href="{!!$url!!}">Get Authenticated!</a>
    @endif
</div>
@endsection

