@extends('layouts.app')

@section('content')

<div class = "row center-align">
        <a href="{{route('home')}}" class = "btn red waves">Home</a>
        {{-- If no errors, display create/find button --}}
        <a href="{{route('servers')}}" class = "btn red waves">Servers</a>

</div>
<div class = "row center-align white">
    <form class="col s10 offset-s1" method = "POST" action = "{{route('createServer')}}">
        {{ csrf_field() }}
        <div class="row gap">
            <div class="input-field col s6">
                <input placeholder="192.168.2.189" id="address" type="text" name = "address" class="validate">
                <label for="address">IP Address</label>
            </div>
            <div class="input-field col s6">
                <input id="hostname" type="text" name = "hostname" placeholder="sboxdc"class="validate">
                <label for="hostname">Hostname</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <textarea placeholder = "Example: Seedbox domain controller!" class = "materialize-textarea" name="description" id="description" cols="10" rows="3"></textarea>
                <label for="description">Description</label>
            </div>
        </div>
        <div class = "row">
            <div class = "right-align">
                <button type = "submit" class = "btn large black waves-effect waves-light">Create</button>
            </div>
        </div>
    </form>
</div>
@endsection

