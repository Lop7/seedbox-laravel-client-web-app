<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Seedbox Test</title>
    <!-- Compiled and minified Materialize-CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Do+Hyeon|Catamaran" rel="stylesheet">
    <link href="{{ asset('css/base.css') }}" rel="stylesheet">
</head>

<body class="body-style">
    <div class="container">
        <div class="row">
            <div class="col m8 offset-m2">
                <div class="row">
                    <h5 class="center-align title">Seedbox Test</h5>
                </div>
                @yield('content')
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
</body>

</html>