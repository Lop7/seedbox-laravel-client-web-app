@extends('layouts.app')

@section('content')
<div class = " row warning center-align">
    Sorry, you aren't authorized to view this page! 
</div>
<div class = "row center-align">
<a href="{{route('home')}}" class = "btn red waves">Home</a>
</div>
@endsection

