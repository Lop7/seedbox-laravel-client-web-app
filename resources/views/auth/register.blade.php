@extends('layouts.app')

@section('content')
<!-- Register -->
<form action="{{ route('register') }}" method="PoSt">
    {{ csrf_field() }}
    <div class="card large">
        <div class="row card-content">
            <div class="col m4 offset-m4">
                <span class="card-title center-align">Register</span>
            </div>
            <div class="row shrink-row">
                <div class="input-field col m10 offset-m1 s12">
                    <input id="name" type="text" name = "name" class="validate">
                    <label for="name">Name</label>
                </div>
            </div>
            <div class="row shrink-row">
                <div class="input-field col m10 offset-m1 s12">
                    <input id="email" type="email" name = "email" class="validate">
                    <label for="email">E-mail</label>
                </div>
            </div>
            <div class="row shrink-row">
                <div class="input-field col m10 offset-m1 s12">
                    <input id="password" type="password" name="password" class="validate">
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="row shrink-row">
            <div class="input-field col m10 offset-m1 s12">
                <input id="password-confirm" type="password" name="password_confirmation" class="validate">
                <label for="password">Password</label>
            </div>
        </div>
            <div class="row shrink-row">
                <div class="col m4 push-m1">
                    <div class="left-align">
                        <button class="btn" type = "submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
