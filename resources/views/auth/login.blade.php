@extends('layouts.app')

@section('content')
<!-- Login -->
<form action="{{ route('login') }}" method="POST">
    {{ csrf_field() }}
    <div class="card medium">
        <div class="row card-content">
            <div class="col m4 offset-m4">
                <span class="card-title center-align">Login</span>
            </div>
            <div class="row">
                <div class="input-field col m10 offset-m1 s12">
                    <input id="email" name = "email" type="email" class="validate">
                    <label for="email">E-mail</label>
                </div>
            </div>
            <div class="row shrink-row">
                <div class="input-field col m10 offset-m1 s12">
                    <input id="password" name = "password" type="password" class="validate">
                    <label for="password">Password</label>
                </div>
            </div>
            @if ($errors->any())
            <div class = "row">
                @foreach ($errors->all() as $error)
                    <div class = "center-align helper-text warning">{{$error}}</div>
                @endforeach
            </div>
            @endif
            <div class="row">
                <div class="col m4 push-m1">
                    <div class="left-align">
                        <button class="btn" type = "submit">Login</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
