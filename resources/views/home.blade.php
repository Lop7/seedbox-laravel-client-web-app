@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col m12">
            <div class="card">
                <div class="card-title">
                    <div class = "center-align">Welcome!</div>
                </div>

                <div class="card-content">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            {{Auth::user()->name}}
                        </div>
                    @endif
                    <div class = "row">
                        <div class = "col m10 offset-m1">
                            <div class ="row">
                                <div class = "center-align">
                                    Hey {{Auth::user()->name}}, 
                                    Lets get started! Click below.
                                </div>
                            </div>
                            <div class = "row">
                                <div class = "center-align">
                                    <a class = "btn indigo lighten-1"href="{{route('manage')}}">Authenticate!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
