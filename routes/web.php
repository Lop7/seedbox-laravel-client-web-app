<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/restricted','HomeController@restricted')->name('restricted');
Route::get('/logout', 'HomeController@logout')->name('logout');

Route::get('/manage', 'AdminController@index')->name('manage');
// Calling for auth_code via auth0 grant service
Route::get('/manage/oauthgrant', 'AdminController@oauthGrant')->name('grant');
// Calling for jwt access_token via auth0 jwt service
Route::get('/manage/obtaintoken','AdminController@oauthAccessTkn')->name("oauthAccessTkn");
// Calling our service to get data / verify jwt / get all servers
Route::get('/manage/servers', 'AdminController@servers')->name("servers");
Route::get('/manage/servers/create', 'AdminController@createForm')->name("createForm");
Route::post('/manage/servers/create', 'AdminController@createServer')->name("createServer");
Route::get('/manage/servers/delete/{id}', 'AdminController@deleteServer')->name("deleteServer");
Route::get('/manage/servers/update/{id}', 'AdminController@updateForm')->name("updateForm");
Route::post('/manage/servers/update/{id}', 'AdminController@updateServer')->name("updateServer");