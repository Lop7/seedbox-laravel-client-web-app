<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Cookie;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AdminController extends Controller
{   
    public function __construct(){
        // Uses built in auth to handle visitors that are not logged in
        $this->middleware('auth');
        // Redirects anyone without access to the Admin section
        $this->middleware(function ($request, $next) {
            $thisUser = Auth::user();
            if(!$thisUser->hasRole('admin')){
                return redirect()->action('HomeController@restricted');
            }
            return $next($request);
        });
    }
    public function index(){
        // "scope=".           env("SCOPE") . "&" .
        $data['url'] =          env("AUTH0_URL") . 
            "audience=".        env("AUDIENCE") . "&" .
            "response_type=".   env("RESPONSE_TYPE") ."&" .
            "client_id=".       env("CLIENT_ID") ."&" .
            "redirect_uri=".    env("REDIRECT_URI");
        return view("oauth")->with($data);
    }
    public function oauthGrant(Request $request){
        $currentUser = User::find(Auth::user()->id);
        if($request->has("code")){
            $currentUser->code = $request->query("code");
            $currentUser->save();
            return redirect()->route('oauthAccessTkn');
        }
        return "Something went wrong";
    }
    public function oauthAccessTkn(){
        $currentUser = User::find(Auth::user()->id);
        $curl = curl_init();
        $grant_type = env("GRANT_TYPE");
        $client_id = env("CLIENT_ID");
        $client_secret = env("CLIENT_SECRET");
        $code = $currentUser->code;
        $redirect_uri = env("REDIRECT_URI");
        $jsonObj = [
            "grant_type" => $grant_type, "client_id" => $client_id, 
            "client_secret" => $client_secret, "code" => $code, 
            "redirect_uri" => $redirect_uri,
        ];
        // curl_setopt($curl, CURLOPT_PORT, 3001);
        curl_setopt_array($curl, array(
            CURLOPT_URL => getenv('OAUTH_TOKEN_URL'),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($jsonObj),
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            return;
        } 
        $authObj = json_decode($response);
        if(!isset($authObj->access_token))
            return "Something went wrong!";
        $token = $authObj->access_token;
        // Set cookie, as auth0 prescribes
        Cookie::queue(
            'access_token',
            $authObj->access_token,
            864000, // expires in 24 hrs
            null,
            null,
            false
        );
        // redirect to server mgmt page?
        return redirect()->route('servers');
    }
    public function servers(){
        // Gets a list of the servers! Get sets up UI for CRUD!
        // Check to see if jwt cookie set!
        // If not redirect to manage page for auth0 flow 
        if(!isset($_COOKIE["access_token"]))
            return "TOKEN NOT SET";
        $access_token = $_COOKIE["access_token"];
        $client = new Client();
        try {
            $res = $client->request('GET', getenv("SERVICE_HOST").'/auth/servers', [
                'headers' => [
                    'Authorization' =>"Bearer ".$access_token,   
                    'Content-type' => 'application/json'
            ]]);
        }
        catch (RequestException $e) {
            $data['error'] = true;
            \Log::error( Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                \Log::error($e->getResponse());
            }
        }
        $data['error'] = null;
        $servers =  json_decode($res->getBody());
        $data['servers'] = $servers;
        return view('servers')->with($data);
    }
    public function createForm(){
        return view("createForm");
    }
    // findServer ... Gets a list of the servers! Get sets up UI for CRUD!
    public function findServer($id){
        // Check to see if jwt cookie set!
        // If not redirect to manage page for auth0 flow 
        if(!isset($_COOKIE["access_token"]))
            return "TOKEN NOT SET";

        $access_token = $_COOKIE["access_token"];
        $client = new Client();

        try {
            $res = $client->request('GET', getenv("SERVICE_HOST").'/auth/servers/'.$id, [
                'headers' => [
                    'Authorization' =>"Bearer ".$access_token,   
                    'Content-type' => 'application/json'
            ]]);
        }
        catch (RequestException $e) {
            $data['error'] = true;
            \Log::error( Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                \Log::error($e->getResponse());
            }
        }
        
        $data['error'] = null;
        $server =  json_decode($res->getBody());
        $data['server'] = $server;
        return $data;
    }
    // createServer ... Creates a `server` in our web service database
    public function createServer(Request $request){
        if(!isset($_COOKIE["access_token"]))
            return "TOKEN NOT SET";
        $access_token = $_COOKIE["access_token"];
        $client = new Client();
        try {
            $res = $client->request('POST', getenv("SERVICE_HOST").'/auth/servers', [
                'json' => [
                    'hostname'=> $request->input('hostname'),
                    'address'=> $request->input('address'),
                    'description'=> $request->input('description'),
                    'updated_by' => Auth::user()->email,
                ],
                'headers' => [
                    'Authorization' =>"Bearer ".$access_token,   
                    'Content-type' => 'application/json'
            ]]);
        }
        catch (RequestException $e) {
            $data['error'] = true;
            \Log::error( Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                \Log::error($e->getResponse());
                // echo Psr7\str($e->getResponse());
            }
        }
        $data['error'] = null;
        $data['msg'] = $res->getBody();
        return redirect()->route('servers');
    }
    // createServer ... Deletes a `server` in our web service database
    public function deleteServer($id){
        if(!isset($_COOKIE["access_token"]))
            return "TOKEN NOT SET";
        $access_token = $_COOKIE["access_token"];
        $client = new Client();
        try {
            $res = $client->request('delete', getenv("SERVICE_HOST").'/auth/servers/'.$id, [
                'headers' => [
                    'Authorization' =>"Bearer ".$access_token,   
                    'Content-type' => 'application/json'
            ]]);
        }
        catch (RequestException $e) {
            $data['error'] = true;
            \Log::error( Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                \Log::error($e->getResponse());
            }
        }
        $data['error'] = null;
        $data['msg'] = $res->getBody();
        return redirect()->route('servers');
    }
    // updateServer ... Updates a `server` in our web service database
    public function updateForm($id){
        $data = $this->findServer($id);
        return view('updateForm')->with($data);
    }
    public function updateServer($id, Request $request){
        if(!isset($_COOKIE["access_token"]))
            return "TOKEN NOT SET";
        $access_token = $_COOKIE["access_token"];
        $client = new Client();
        try {
            $res = $client->request('put', getenv("SERVICE_HOST").'/auth/servers/' . $id, [
                'json' => [
                    'hostname'=> $request->input('hostname'),
                    'address'=> $request->input('address'),
                    'description'=> $request->input('description'),
                    'updated_by' => Auth::user()->email,
                ],
                'headers' => [
                    'Authorization' =>"Bearer ".$access_token,   
                    'Content-type' => 'application/json'
            ]]);
        }
        catch (RequestException $e) {
            $data['error'] = true;
            \Log::error( Psr7\str($e->getRequest()));
            if ($e->hasResponse()) {
                \Log::error($e->getResponse());
            }
        }
        $data['error'] = null;
        $data['msg'] = $res->getBody();
        return redirect()->route('servers');
    }
}
