# Seedbox Test Project
#### Welcome!
This is a web app written to specifically send requests to our web service [written in Go. ](https://bitbucket.org/Lop7/seedbox-go-service/src/master/) I will give a quick explanation on how to get this web app up and running. 

#### Web service / application flow
![Service Flow](https://s22.postimg.cc/u0xhvwpr5/service-flow.png)


# Requirements 
### Here is what you will need to get this service running 

 - PHP 7+
 - MySQL Server (You can share instances with the web service for demo purposes) and DB set up.
 - An OAuth2 service account (*you can use my .env file, which includes my service credentials*)
 - A Machine (*I am currently running this with Windows, but used a virtual Ubuntu box*)
 - A GMail account or willingness to create a user account with Auth0 (you will be prompted on the first leg of the Flow)

 *I personally think it would be easier to use a VM or some unix based instance but the choice is yours.* 

## Installation
*Assuming you have the above requirements...*
1. Clone this repository, and download the .env file I attached to the e-mail. Feel free to create your own Environment Vars if you are comfortable with auth0.
2. Download the following custom packages (*dependencies*) via these commands in your command prompt, whether it be Bash, Cmd, or GitBash :

```shell
    $ cd /wherever-you-cloned-this-repo
    $ composer install
    $ php artisan migrate # assuming you have a db setup
    $ php artisan db:seed
```
## Why Laravel + OAuth2?

Laravel allows developers to get writing very quickly *as you probably know*. I also wanted to score some points for using PHP.  

I went with OAuth because I had never tried it before and it has been getting popular lately. It is also fairly secure and is really well documented. 

## How does the app work, anyway?

1. The administrative zone is essentially the Laravel web app. I am using a Role based system, and only Administrators can create/modify/delete/read servers. 
2. It communicated with the web service and OAuth2 servers via the "Authorization Code Flow", you can read more about it [here](https://developer.okta.com/blog/2018/04/10/oauth-authorization-code-grant-type)

## What's next?

CI, build deployments, and a cleaner UI. There are also a few potential errors that I still need to catch, either way, the app is running the main functions. I wish I had more time! Unfortunately I have been busy as of late, so this may have to wait until this weekend (*it is 5 AM as I type*). I typically test as well, but I wanted to meet the deadline given to me. 

#### *If I am missing something please let me know!
# Please check out the sister repo that goes along with this service 
### [Bitbucket link](https://bitbucket.org/Lop7/seedbox-go-service/src/master/)
### Also check out my last mini project with a pretty build flow
[GitHub link](https://github.com/masonmr/small-ecometrica-demo)

