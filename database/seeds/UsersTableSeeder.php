<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::find(1);
        $user = User::create([
            'name' => 'admin',
            'email' => 'test@test.com',
            'password' => Hash::make('password'),
            'code' => null
        ]);
        $user->attachRole($admin); // parameter can be an Role object, array, or id
    }
}
