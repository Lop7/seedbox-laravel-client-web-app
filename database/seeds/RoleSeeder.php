<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Administrator',
                'description' => 'Allows access to service'
            ],
            [
                'name' => 'member',
                'display_name' => 'Member',
                'description' => 'No access to any services, but can request access.'
            ],
        ];
        foreach ($roles as $key => $val) {
            Role::create($val);
        }
    }
}
